package com.example.implicitintents

import android.content.Intent
import android.content.pm.PackageManager
import android.net.Uri
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.provider.MediaStore
import android.util.Log
import android.view.View
import android.widget.EditText
import androidx.core.app.ShareCompat
import java.net.URI

class MainActivity : AppCompatActivity() {
    lateinit private var mWebSiteEditText: EditText
    lateinit private var mLocationEditText: EditText
    lateinit private var mShareTextEditText: EditText

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        mWebSiteEditText = findViewById(R.id.website_edittext)
        mLocationEditText = findViewById(R.id.location_edittext)
        mShareTextEditText = findViewById(R.id.share_edittext)
    }

    fun openWebsite(view: View) {
        // Get the URL text
        val url: String = mWebSiteEditText.text.toString()
        // parse the URI and create the intent
        val webpage: Uri = Uri.parse(url)
        val intent = Intent(Intent.ACTION_VIEW, webpage)
        // find an activity to handle the intent and start intent
        if (intent.resolveActivity(packageManager) != null){
            startActivity(intent)
        }
        else{
            Log.d("Implicit Intents", "Can't handle this")
        }
    }

    fun openLocation(view: View) {
        val loc: String = mLocationEditText.text.toString()
        val addressUri: Uri = Uri.parse("geo:0,0?q=" + loc)
        val intent = Intent(Intent.ACTION_VIEW, addressUri)
        if (intent.resolveActivity(packageManager) != null){
            startActivity(intent)
        }
        else{
            Log.d("Implicit Intents", "Can't handle this")
        }
    }

    fun shareText(view: View) {
        val txt: String = mShareTextEditText.text.toString()
        val mimeType: String = "text/plain"
        ShareCompat.IntentBuilder
            .from(this)
            .setType(mimeType)
            .setChooserTitle(getString(R.string.chooser_title))
            .setText(txt)
            .startChooser()
    }

    fun takePicture(view: View) {
        val intent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
        startActivity(intent)
    }
}